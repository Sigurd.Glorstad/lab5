package cellular;

import cellular.gui.CellAutomataGUI;
import datastructure.Grid;

public class Main {

	public static void main(String[] args) {

		//ICellAutomaton ca = new LangtonsAnt(50, 50, "RRLLLRLLLRRR");
		//ICellAutomaton ca = new SeedsAutomaton(50, 50);
		ICellAutomaton ca = new GameOfLife(50, 50);

		CellAutomataGUI.run(ca);

		Grid<Integer> myGrid = new Grid<Integer>(20, 20, null);
	}

}
